﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AllusionFileSystem
{
    public partial class RegisterWatcher : Form
    {
        private String watchableName;

        public RegisterWatcher(String watchableName) {
            InitializeComponent();
            this.watchableName = watchableName;
        }

        private void btnRegister_Click(object sender, EventArgs e) {
            if (checkBoxCreate.Checked) {
                AFSForm.QueueCommand("register " + watchableName + " creation");
            } else {

            }
            if (checkBoxDelete.Checked) {
                AFSForm.QueueCommand("register " + watchableName + " deletion");
            } else {

            }
            if (checkBoxRename.Checked) {
                AFSForm.QueueCommand("register " + watchableName + " rename");
            } else {

            }
            if (checkBoxChange.Checked) {
                AFSForm.QueueCommand("register " + watchableName + " modification");
            } else {

            }
            this.Close();
        }
    }
}
