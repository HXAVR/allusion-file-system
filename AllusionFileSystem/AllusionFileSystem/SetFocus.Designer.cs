﻿namespace AllusionFileSystem
{
    partial class SetFocus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetFocus));
            this.pictureBoxGoodDir = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxDirectory = new System.Windows.Forms.TextBox();
            this.buttonSelectDir = new System.Windows.Forms.Button();
            this.btnSet = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGoodDir)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxGoodDir
            // 
            this.pictureBoxGoodDir.Location = new System.Drawing.Point(469, 4);
            this.pictureBoxGoodDir.Name = "pictureBoxGoodDir";
            this.pictureBoxGoodDir.Size = new System.Drawing.Size(25, 25);
            this.pictureBoxGoodDir.TabIndex = 9;
            this.pictureBoxGoodDir.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Directory:";
            // 
            // textBoxDirectory
            // 
            this.textBoxDirectory.Location = new System.Drawing.Point(70, 6);
            this.textBoxDirectory.Name = "textBoxDirectory";
            this.textBoxDirectory.Size = new System.Drawing.Size(366, 20);
            this.textBoxDirectory.TabIndex = 8;
            this.textBoxDirectory.TextChanged += new System.EventHandler(this.textBoxDirectory_TextChanged);
            // 
            // buttonSelectDir
            // 
            this.buttonSelectDir.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonSelectDir.Location = new System.Drawing.Point(442, 4);
            this.buttonSelectDir.Name = "buttonSelectDir";
            this.buttonSelectDir.Size = new System.Drawing.Size(21, 23);
            this.buttonSelectDir.TabIndex = 7;
            this.buttonSelectDir.Text = ".";
            this.buttonSelectDir.UseVisualStyleBackColor = true;
            this.buttonSelectDir.Click += new System.EventHandler(this.buttonSelectDir_Click);
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(361, 32);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(75, 23);
            this.btnSet.TabIndex = 10;
            this.btnSet.Text = "Set";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // SetFocus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 59);
            this.Controls.Add(this.btnSet);
            this.Controls.Add(this.pictureBoxGoodDir);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxDirectory);
            this.Controls.Add(this.buttonSelectDir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetFocus";
            this.Text = "Set Directory";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGoodDir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxGoodDir;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxDirectory;
        private System.Windows.Forms.Button buttonSelectDir;
        private System.Windows.Forms.Button btnSet;
    }
}