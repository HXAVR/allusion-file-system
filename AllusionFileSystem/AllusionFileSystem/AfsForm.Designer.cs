﻿namespace AllusionFileSystem
{
    partial class AFSForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AFSForm));
            this.visualInterfaceTabPage = new System.Windows.Forms.TabPage();
            this.buttonOptions = new System.Windows.Forms.Button();
            this.btnRegisterEvent = new System.Windows.Forms.Button();
            this.btnSetState = new System.Windows.Forms.Button();
            this.btnSetExtent = new System.Windows.Forms.Button();
            this.btnSetFocus = new System.Windows.Forms.Button();
            this.btnNewWatcher = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listBoxNotifications = new System.Windows.Forms.ListBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.observerGroupBox = new System.Windows.Forms.GroupBox();
            this.passiveCountlbl = new System.Windows.Forms.Label();
            this.obInfoPassiveCountLabel = new System.Windows.Forms.Label();
            this.activeCountlbl = new System.Windows.Forms.Label();
            this.obInfoActiveCountLabel = new System.Windows.Forms.Label();
            this.watcherCountlbl = new System.Windows.Forms.Label();
            this.obInfoWatchableCountLabel = new System.Windows.Forms.Label();
            this.appTabControl = new System.Windows.Forms.TabControl();
            this.consoleTabPage = new System.Windows.Forms.TabPage();
            this.consoleTextBox = new System.Windows.Forms.TextBox();
            this.consoleRichTextBox = new System.Windows.Forms.RichTextBox();
            this.visualInterfaceTabPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.observerGroupBox.SuspendLayout();
            this.appTabControl.SuspendLayout();
            this.consoleTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // visualInterfaceTabPage
            // 
            this.visualInterfaceTabPage.Controls.Add(this.buttonOptions);
            this.visualInterfaceTabPage.Controls.Add(this.btnRegisterEvent);
            this.visualInterfaceTabPage.Controls.Add(this.btnSetState);
            this.visualInterfaceTabPage.Controls.Add(this.btnSetExtent);
            this.visualInterfaceTabPage.Controls.Add(this.btnSetFocus);
            this.visualInterfaceTabPage.Controls.Add(this.btnNewWatcher);
            this.visualInterfaceTabPage.Controls.Add(this.groupBox1);
            this.visualInterfaceTabPage.Controls.Add(this.dataGridView1);
            this.visualInterfaceTabPage.Controls.Add(this.observerGroupBox);
            this.visualInterfaceTabPage.Location = new System.Drawing.Point(4, 22);
            this.visualInterfaceTabPage.Name = "visualInterfaceTabPage";
            this.visualInterfaceTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.visualInterfaceTabPage.Size = new System.Drawing.Size(798, 460);
            this.visualInterfaceTabPage.TabIndex = 1;
            this.visualInterfaceTabPage.Text = "Graphical Representation";
            this.visualInterfaceTabPage.UseVisualStyleBackColor = true;
            // 
            // buttonOptions
            // 
            this.buttonOptions.Location = new System.Drawing.Point(715, 6);
            this.buttonOptions.Name = "buttonOptions";
            this.buttonOptions.Size = new System.Drawing.Size(75, 23);
            this.buttonOptions.TabIndex = 4;
            this.buttonOptions.Text = "Options";
            this.buttonOptions.UseVisualStyleBackColor = true;
            // 
            // btnRegisterEvent
            // 
            this.btnRegisterEvent.Location = new System.Drawing.Point(267, 34);
            this.btnRegisterEvent.Name = "btnRegisterEvent";
            this.btnRegisterEvent.Size = new System.Drawing.Size(92, 23);
            this.btnRegisterEvent.TabIndex = 3;
            this.btnRegisterEvent.Text = "Register Event";
            this.btnRegisterEvent.UseVisualStyleBackColor = true;
            this.btnRegisterEvent.Click += new System.EventHandler(this.btnRegisterEvent_Click);
            // 
            // btnSetState
            // 
            this.btnSetState.Location = new System.Drawing.Point(169, 34);
            this.btnSetState.Name = "btnSetState";
            this.btnSetState.Size = new System.Drawing.Size(92, 23);
            this.btnSetState.TabIndex = 3;
            this.btnSetState.Text = "Set Stance";
            this.btnSetState.UseVisualStyleBackColor = true;
            this.btnSetState.Click += new System.EventHandler(this.btnSetState_Click);
            // 
            // btnSetExtent
            // 
            this.btnSetExtent.Location = new System.Drawing.Point(169, 62);
            this.btnSetExtent.Name = "btnSetExtent";
            this.btnSetExtent.Size = new System.Drawing.Size(92, 23);
            this.btnSetExtent.TabIndex = 3;
            this.btnSetExtent.Text = "Set Extent";
            this.btnSetExtent.UseVisualStyleBackColor = true;
            this.btnSetExtent.Click += new System.EventHandler(this.btnSetExtent_Click);
            // 
            // btnSetFocus
            // 
            this.btnSetFocus.Location = new System.Drawing.Point(267, 6);
            this.btnSetFocus.Name = "btnSetFocus";
            this.btnSetFocus.Size = new System.Drawing.Size(92, 23);
            this.btnSetFocus.TabIndex = 3;
            this.btnSetFocus.Text = "Set Focus";
            this.btnSetFocus.UseVisualStyleBackColor = true;
            this.btnSetFocus.Click += new System.EventHandler(this.btnSetFocus_Click);
            // 
            // btnNewWatcher
            // 
            this.btnNewWatcher.Location = new System.Drawing.Point(169, 6);
            this.btnNewWatcher.Name = "btnNewWatcher";
            this.btnNewWatcher.Size = new System.Drawing.Size(92, 23);
            this.btnNewWatcher.TabIndex = 3;
            this.btnNewWatcher.Text = "New Watcher";
            this.btnNewWatcher.UseVisualStyleBackColor = true;
            this.btnNewWatcher.Click += new System.EventHandler(this.btnNewWatcher_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listBoxNotifications);
            this.groupBox1.Location = new System.Drawing.Point(7, 335);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(783, 122);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Watcher Notifications";
            // 
            // listBoxNotifications
            // 
            this.listBoxNotifications.FormattingEnabled = true;
            this.listBoxNotifications.Location = new System.Drawing.Point(7, 20);
            this.listBoxNotifications.Name = "listBoxNotifications";
            this.listBoxNotifications.Size = new System.Drawing.Size(770, 95);
            this.listBoxNotifications.TabIndex = 0;
            this.listBoxNotifications.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8});
            this.dataGridView1.Location = new System.Drawing.Point(9, 92);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(781, 236);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.TabStop = false;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Name";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 80;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Directory";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 300;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Extent";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 85;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Creation";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 52;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Deletion";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 52;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Rename";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 53;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Change";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 50;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Stance";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 66;
            // 
            // observerGroupBox
            // 
            this.observerGroupBox.Controls.Add(this.passiveCountlbl);
            this.observerGroupBox.Controls.Add(this.obInfoPassiveCountLabel);
            this.observerGroupBox.Controls.Add(this.activeCountlbl);
            this.observerGroupBox.Controls.Add(this.obInfoActiveCountLabel);
            this.observerGroupBox.Controls.Add(this.watcherCountlbl);
            this.observerGroupBox.Controls.Add(this.obInfoWatchableCountLabel);
            this.observerGroupBox.Location = new System.Drawing.Point(8, 6);
            this.observerGroupBox.Name = "observerGroupBox";
            this.observerGroupBox.Size = new System.Drawing.Size(155, 79);
            this.observerGroupBox.TabIndex = 0;
            this.observerGroupBox.TabStop = false;
            this.observerGroupBox.Text = "Observer Information:";
            // 
            // passiveCountlbl
            // 
            this.passiveCountlbl.AutoSize = true;
            this.passiveCountlbl.Location = new System.Drawing.Point(88, 56);
            this.passiveCountlbl.Name = "passiveCountlbl";
            this.passiveCountlbl.Size = new System.Drawing.Size(42, 13);
            this.passiveCountlbl.TabIndex = 0;
            this.passiveCountlbl.Text = "VALUE";
            // 
            // obInfoPassiveCountLabel
            // 
            this.obInfoPassiveCountLabel.AutoSize = true;
            this.obInfoPassiveCountLabel.Location = new System.Drawing.Point(7, 56);
            this.obInfoPassiveCountLabel.Name = "obInfoPassiveCountLabel";
            this.obInfoPassiveCountLabel.Size = new System.Drawing.Size(78, 13);
            this.obInfoPassiveCountLabel.TabIndex = 0;
            this.obInfoPassiveCountLabel.Text = "Passive Count:";
            // 
            // activeCountlbl
            // 
            this.activeCountlbl.AutoSize = true;
            this.activeCountlbl.Location = new System.Drawing.Point(88, 38);
            this.activeCountlbl.Name = "activeCountlbl";
            this.activeCountlbl.Size = new System.Drawing.Size(42, 13);
            this.activeCountlbl.TabIndex = 0;
            this.activeCountlbl.Text = "VALUE";
            // 
            // obInfoActiveCountLabel
            // 
            this.obInfoActiveCountLabel.AutoSize = true;
            this.obInfoActiveCountLabel.Location = new System.Drawing.Point(7, 38);
            this.obInfoActiveCountLabel.Name = "obInfoActiveCountLabel";
            this.obInfoActiveCountLabel.Size = new System.Drawing.Size(70, 13);
            this.obInfoActiveCountLabel.TabIndex = 0;
            this.obInfoActiveCountLabel.Text = "Active count:";
            // 
            // watcherCountlbl
            // 
            this.watcherCountlbl.AutoSize = true;
            this.watcherCountlbl.Location = new System.Drawing.Point(88, 20);
            this.watcherCountlbl.Name = "watcherCountlbl";
            this.watcherCountlbl.Size = new System.Drawing.Size(42, 13);
            this.watcherCountlbl.TabIndex = 0;
            this.watcherCountlbl.Text = "VALUE";
            // 
            // obInfoWatchableCountLabel
            // 
            this.obInfoWatchableCountLabel.AutoSize = true;
            this.obInfoWatchableCountLabel.Location = new System.Drawing.Point(7, 20);
            this.obInfoWatchableCountLabel.Name = "obInfoWatchableCountLabel";
            this.obInfoWatchableCountLabel.Size = new System.Drawing.Size(81, 13);
            this.obInfoWatchableCountLabel.TabIndex = 0;
            this.obInfoWatchableCountLabel.Text = "Watcher count:";
            // 
            // appTabControl
            // 
            this.appTabControl.Controls.Add(this.consoleTabPage);
            this.appTabControl.Controls.Add(this.visualInterfaceTabPage);
            this.appTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.appTabControl.Location = new System.Drawing.Point(0, 0);
            this.appTabControl.Name = "appTabControl";
            this.appTabControl.SelectedIndex = 0;
            this.appTabControl.Size = new System.Drawing.Size(806, 486);
            this.appTabControl.TabIndex = 0;
            this.appTabControl.TabStop = false;
            // 
            // consoleTabPage
            // 
            this.consoleTabPage.BackColor = System.Drawing.SystemColors.WindowText;
            this.consoleTabPage.Controls.Add(this.consoleTextBox);
            this.consoleTabPage.Controls.Add(this.consoleRichTextBox);
            this.consoleTabPage.Location = new System.Drawing.Point(4, 22);
            this.consoleTabPage.Name = "consoleTabPage";
            this.consoleTabPage.Size = new System.Drawing.Size(798, 460);
            this.consoleTabPage.TabIndex = 2;
            this.consoleTabPage.Text = "Console";
            // 
            // consoleTextBox
            // 
            this.consoleTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.consoleTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.consoleTextBox.BackColor = System.Drawing.SystemColors.WindowText;
            this.consoleTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.consoleTextBox.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consoleTextBox.ForeColor = System.Drawing.SystemColors.Window;
            this.consoleTextBox.Location = new System.Drawing.Point(0, 434);
            this.consoleTextBox.Name = "consoleTextBox";
            this.consoleTextBox.Size = new System.Drawing.Size(798, 26);
            this.consoleTextBox.TabIndex = 0;
            this.consoleTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.consoleTextBox_KeyDown);
            // 
            // consoleRichTextBox
            // 
            this.consoleRichTextBox.BackColor = System.Drawing.SystemColors.WindowText;
            this.consoleRichTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.consoleRichTextBox.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consoleRichTextBox.ForeColor = System.Drawing.SystemColors.Window;
            this.consoleRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.consoleRichTextBox.Name = "consoleRichTextBox";
            this.consoleRichTextBox.ReadOnly = true;
            this.consoleRichTextBox.Size = new System.Drawing.Size(798, 434);
            this.consoleRichTextBox.TabIndex = 10;
            this.consoleRichTextBox.TabStop = false;
            this.consoleRichTextBox.Text = "";
            // 
            // AFSForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 486);
            this.Controls.Add(this.appTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AFSForm";
            this.Text = "Allusion File System";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AFSForm_FormClosing);
            this.Load += new System.EventHandler(this.AFSForm_Load);
            this.visualInterfaceTabPage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.observerGroupBox.ResumeLayout(false);
            this.observerGroupBox.PerformLayout();
            this.appTabControl.ResumeLayout(false);
            this.consoleTabPage.ResumeLayout(false);
            this.consoleTabPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage visualInterfaceTabPage;
        private System.Windows.Forms.TabControl appTabControl;
        private System.Windows.Forms.TabPage consoleTabPage;
        private System.Windows.Forms.TextBox consoleTextBox;
        private System.Windows.Forms.RichTextBox consoleRichTextBox;
        private System.Windows.Forms.GroupBox observerGroupBox;
        private System.Windows.Forms.Label obInfoWatchableCountLabel;
        private System.Windows.Forms.Label obInfoPassiveCountLabel;
        private System.Windows.Forms.Label obInfoActiveCountLabel;
        public System.Windows.Forms.Label watcherCountlbl;
        public System.Windows.Forms.Label passiveCountlbl;
        public System.Windows.Forms.Label activeCountlbl;
        private System.Windows.Forms.DataGridViewTextBoxColumn getFileSystemWatcherDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn getNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn getFocusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn getExtentDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn getSessionNotificationCountDataGridViewTextBoxColumn;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox listBoxNotifications;
        private System.Windows.Forms.Button btnSetState;
        private System.Windows.Forms.Button btnSetExtent;
        private System.Windows.Forms.Button btnNewWatcher;
        private System.Windows.Forms.Button btnRegisterEvent;
        private System.Windows.Forms.Button btnSetFocus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.Button buttonOptions;

    }
}

