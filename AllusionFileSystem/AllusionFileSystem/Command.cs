﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllusionFileSystem
{
    public class Command
    {
        /// <summary>
        /// Name of the method the command calls upon invocation.
        /// </summary>
        private String methodName;
        /// <summary>
        /// Name of the command.
        /// </summary>
        private String commandName;
        /// <summary>
        /// Command usage.
        /// </summary>
        private String usage;
        /// <summary>
        /// Minimum passable parameters.
        /// </summary>
        private Int16 minParameterCount;
        /// <summary>
        /// Maximum passable parameters.
        /// </summary>
        private Int16 maxParameterCount;
        /// <summary>
        /// Determines if the command is a debug command.
        /// </summary>
        private Boolean isDebugCommand = false;

        /// <summary>
        /// Instantiates a new Command.
        /// </summary>
        /// <param name="parCommandName">Command name</param>
        /// <param name="parMethodName">Invokeable method</param>
        /// <param name="parUsage">Command usage</param>
        /// <param name="parMinParamCount">Minimum parameter count</param>
        /// <param name="parMaxParamCount">Maximum parameter count</param>
        public Command(String parCommandName, String parMethodName, String parUsage, Int16 parMinParamCount, Int16 parMaxParamCount) {
            this.commandName = parCommandName;
            this.methodName = parMethodName;
            this.usage = parUsage;
            this.minParameterCount = parMinParamCount;
            this.maxParameterCount = parMaxParamCount;
        }

        /// <summary>
        /// Gets the Invokeable name.
        /// </summary>
        /// <returns>methodName</returns>
        public String GetMethodName() {
            return methodName;
        }

        /// <summary>
        /// Gets the command name.
        /// </summary>
        /// <returns>commandName</returns>
        public String GetCommandName() {
            return commandName;
        }

        /// <summary>
        /// Gets the command usage.
        /// </summary>
        /// <returns>usage</returns>
        public String GetUsage() {
            return usage;
        }

        /// <summary>
        /// Gets the minimum parameter count.
        /// </summary>
        /// <returns>"minParameterCount"</returns>
        public Int16 GetMinParameterCount() {
            return minParameterCount;
        }

        /// <summary>
        /// Gets the maximum parameter count.
        /// </summary>
        /// <returns>"maxParameterCount"</returns>
        public Int16 GetMaxParameterCount() {
            return maxParameterCount;
        }

        public Boolean IsDebugCommand() {
            return isDebugCommand;
        }

        public Command SetDebugCommand() {
            isDebugCommand = true;
            return this;
        }
    }
}
