﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AllusionFileSystem
{
    public partial class SetStance : Form
    {
        private String watcherName;

        public SetStance(String watcherName) {
            InitializeComponent();
            this.watcherName = watcherName;
        }

        private void btnSet_Click(object sender, EventArgs e) {
            AFSForm.QueueCommand("setstance " + watcherName + " " + comboBoxStance.SelectedItem.ToString());
            AFSForm.QueueConsolePost(new ConsolePost("setstance " + watcherName + " " + comboBoxStance.SelectedText, "", Color.Green, true));
            this.Close();
        }
    }
}
