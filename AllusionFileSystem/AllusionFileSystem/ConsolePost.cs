﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace AllusionFileSystem
{
    public class ConsolePost
    {
        /// <summary>
        /// Object or service that posted the message.
        /// </summary>
        public String PostOwner { get; private set; }
        /// <summary>
        /// Post message.
        /// </summary>
        public String Message { get; private set; }
        /// <summary>
        /// Color of the header text.
        /// </summary>
        public Color HeaderColor { get; private set; }
        /// <summary>
        /// Should the post just write a new, empty line.
        /// </summary>
        public Boolean NewLine { get; private set; }
        /// <summary>
        /// Should a new line follow the message.
        /// </summary>
        public Boolean FollowWithNewLine { get; private set; }

        public ConsolePost(String owner, String message, Color headerColor, Boolean newLineFollow) {
            this.PostOwner = owner;
            this.Message = message;
            this.HeaderColor = headerColor;
            FollowWithNewLine = newLineFollow;
            NewLine = false;
        }

        public ConsolePost(Boolean writeNewLine = true) {
            NewLine = writeNewLine;
            FollowWithNewLine = false;
        }
    }
}
