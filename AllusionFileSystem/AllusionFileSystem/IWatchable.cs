﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AllusionFileSystem
{
    interface IWatchable
    {
        void SetStance(EnumStance parStance);
        EnumStance GetStance();
        Notification GetLastNotification();
        void OnCreation(Object parSrc, FileSystemEventArgs parEventArgs);
        void OnDeletion(Object parSrc, FileSystemEventArgs parEventArgs);
        void OnChange(Object parSrc, FileSystemEventArgs parEventArgs);
        void OnRename(Object parSrc, RenamedEventArgs parEventArgs);
    }
}
