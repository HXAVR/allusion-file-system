﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AllusionFileSystem
{
    public partial class SetFocus : Form
    {
        private String watchableName;

        public SetFocus(String watchableName) {
            InitializeComponent();
            this.watchableName = watchableName;
        }

        private void textBoxDirectory_TextChanged(object sender, EventArgs e) {
            if (Directory.Exists(((TextBox)sender).Text)) {
                pictureBoxGoodDir.Image = AllusionFileSystem.Properties.Resources.tick;
            } else {
                pictureBoxGoodDir.Image = AllusionFileSystem.Properties.Resources.cross;
            }
        }

        private void btnSet_Click(object sender, EventArgs e) {
            if (Directory.Exists(textBoxDirectory.Text)) {
                AFSForm.QueueCommand("setfocus " + watchableName + " " + textBoxDirectory.Text);
            }
        }

        private void buttonSelectDir_Click(object sender, EventArgs e) {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowNewFolderButton = true;
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                this.textBoxDirectory.Text = fbd.SelectedPath;
            }
        }
    }
}
