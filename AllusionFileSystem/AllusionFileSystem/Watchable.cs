﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Drawing;

namespace AllusionFileSystem
{
    public class Watchable : IWatchable
    {
        /// <summary>
        /// Name of the watcher.
        /// </summary>
        private String watchableName;
        /// <summary>
        /// Internal File System Watcher object.
        /// </summary>
        private FileSystemWatcher watcher; 
        /// <summary>
        /// Watcher focus.
        /// </summary>
        private String watchableFocus = null;
        /// <summary>
        /// Notification array.
        /// </summary>
        private Notification[] sessionNotifications;
        /// <summary>
        /// Next available position in the notification array.
        /// </summary>
        private Int32 notificationActiveIndex = 0;
        /// <summary>
        /// Last notification.
        /// </summary>
        private Notification lastestNotification = null;
        /// <summary>
        /// Watchers stance.
        /// </summary>
        private EnumStance stance;
        /// <summary>
        /// Should the File System Watcher what lower directories.
        /// </summary>
        private Boolean watchSubDirs = false;
        /// <summary>
        /// How long the watcher should wait during passive waits.
        /// </summary>
        private Int32 passiveWaitTime = 0;
        /// <summary>
        /// Watcher wait result thread.
        /// </summary>
        private Thread waitResultThread = null;
        /// <summary>
        /// Watcher thread wait result.
        /// </summary>
        private WaitForChangedResult threadedResult;
        /// <summary>
        /// Is the watcher watching for creations.
        /// </summary>
        private Boolean hasCreation = false;
        /// <summary>
        /// Is the watcher watching for deletes.
        /// </summary>
        private Boolean hasDeletion = false;
        /// <summary>
        /// Is the watcher watching for rename.
        /// </summary>
        private Boolean hasRename = false;
        /// <summary>
        /// Is the watcher watching for changes.
        /// </summary>
        private Boolean hasChange = false;

        /// <summary>
        /// Instantiates a new named Watcher.
        /// </summary>
        /// <param name="parName">Watcher name</param>
        public Watchable(String parName) {
            sessionNotifications = new Notification[AFSForm.NOTIFICATION_SESSION_SIZE];
            watchableName = parName;
            Update();
        }

        /// <summary>
        /// Instantiates a new named Watcher with a specified focus.
        /// </summary>
        /// <param name="parName">Watcher name</param>
        /// <param name="parFocus">Watcher focus</param>
        public Watchable(String parName, String parFocus) {
            sessionNotifications = new Notification[AFSForm.NOTIFICATION_SESSION_SIZE];
            watchableFocus = parFocus;
            watchableName = parName;
            InitWatcher();
            Update();
        }

        /// <summary>
        /// Sets the watchers focus.
        /// </summary>
        /// <param name="parFocus">Watcher focus</param>
        public void SetFocus(String parFocus) {
            watchableFocus = parFocus;
            InitWatcher();
            Update();
        }
        
        /// <summary>
        /// Sets the File System Watchers extent;
        /// </summary>
        /// <param name="parExtent"></param>
        public void SetExtent(Boolean parExtent) {
            watchSubDirs = parExtent;
            Update();
        }

        /// <summary>
        /// Initiates the File System Wacher
        /// </summary>
        public void InitWatcher() {
            watcher = new FileSystemWatcher();
            UpdateFSW();
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.Attributes | NotifyFilters.Size | NotifyFilters.FileName;
            watcher.EnableRaisingEvents = true;
            Update();
        }

        /// <summary>
        /// Used to initate the File System Watcher unpon reinstaniation.
        /// </summary>
        private void PostInitIntWatcher() {
            UpdateFSW();
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.Attributes | NotifyFilters.Size | NotifyFilters.FileName;
            watcher.EnableRaisingEvents = false;
            Update();
        }

        /// <summary>
        /// Updates the File System Watcher.
        /// </summary>
        private void UpdateFSW() {
            try {
                if (Directory.Exists(watchableFocus)) {
                    watcher.Path = watchableFocus;
                } else {
                    AFSForm.QueueConsolePost(new ConsolePost("AFS: ", "Directory does not exist! Cannot creat watchable!", Color.OrangeRed, true));
                }
                if (watchSubDirs) {
                    watcher.IncludeSubdirectories = watchSubDirs;
                }
            } catch (ArgumentException argex) {
                AFSForm.QueueConsolePost(new ConsolePost("AFS: ", argex.ToString(), Color.OrangeRed, true));
            }
        }

        /// <summary>
        /// Adds an event handler to the File System Watcher.
        /// </summary>
        /// <param name="parEvent">Event type to register.</param>
        /// <returns>Self</returns>
        public Watchable AddEvent(EnumNotificationEventType parEvent) {
            switch (parEvent) {
                case EnumNotificationEventType.ObjectCreated:
                    watcher.Created += new FileSystemEventHandler(OnCreation);
                    hasCreation = true;
                    AFSForm.QueueConsolePost(new ConsolePost("Creation watcher added to " + this.watchableName, "", Color.Magenta, true));
                    break;
                case EnumNotificationEventType.ObjectRemoved:
                    watcher.Deleted += new FileSystemEventHandler(OnDeletion);
                    hasDeletion = true;
                    AFSForm.QueueConsolePost(new ConsolePost("Deletion watcher added to " + this.watchableName, "", Color.Magenta, true));
                    break;
                case EnumNotificationEventType.ObjectRenamed:
                    watcher.Renamed += new RenamedEventHandler(OnRename);
                    hasRename = true;
                    AFSForm.QueueConsolePost(new ConsolePost("Rename watcher added to " + this.watchableName, "", Color.Magenta, true));
                    break;
                case EnumNotificationEventType.ObjectChanged:
                    watcher.Changed += new FileSystemEventHandler(OnChange);
                    hasChange = true;
                    AFSForm.QueueConsolePost(new ConsolePost("Modification watcher added to " + this.watchableName, "", Color.Magenta, true));
                    break;
                default:
                    AFSForm.QueueConsolePost(new ConsolePost("Not an Event option! " + parEvent.ToString(), "", Color.Blue, true));
                    break;
            }
            Update();
            return this;
        }

        /// <summary>
        /// Removes an event from the File System Watcher by creating a new Watcher instance.
        /// </summary>
        /// <param name="parEvent">Notification event type to remove</param>
        public void RemoveEvent(EnumNotificationEventType parEvent) {
            FileSystemWatcher tempWatcher = new FileSystemWatcher(Focus);
            if (parEvent == EnumNotificationEventType.ObjectCreated) {
                hasCreation = false;
                if (hasChange) {
                    tempWatcher.Changed += new FileSystemEventHandler(OnChange);
                }
                if (hasDeletion) {
                    tempWatcher.Deleted += new FileSystemEventHandler(OnDeletion);
                }
                if (hasRename) {
                    tempWatcher.Renamed += new RenamedEventHandler(OnRename);
                }
                AFSForm.QueueConsolePost(new ConsolePost("Creation watcher removed from " + this.watchableName, "", Color.Magenta, true));
            }
            if (parEvent == EnumNotificationEventType.ObjectRemoved) {
                hasDeletion = false;
                if (hasChange) {
                    tempWatcher.Changed += new FileSystemEventHandler(OnChange);
                }
                if (hasRename) {
                    tempWatcher.Renamed += new RenamedEventHandler(OnRename);
                }
                if (hasCreation) {
                    tempWatcher.Created += new FileSystemEventHandler(OnCreation);
                }
                AFSForm.QueueConsolePost(new ConsolePost("Deletion watcher removed from " + this.watchableName, "", Color.Magenta, true));
            }
            if (parEvent == EnumNotificationEventType.ObjectRenamed) {
                hasRename = false;
                if (hasChange) {
                    tempWatcher.Changed += new FileSystemEventHandler(OnChange);
                }
                if (hasDeletion) {
                    tempWatcher.Deleted += new FileSystemEventHandler(OnDeletion);
                }
                if (hasCreation) {
                    tempWatcher.Created += new FileSystemEventHandler(OnCreation);
                }
                AFSForm.QueueConsolePost(new ConsolePost("Rename watcher removed from " + this.watchableName, "", Color.Magenta, true));
            }
            if (parEvent == EnumNotificationEventType.ObjectChanged) {
                hasChange = false;
                if (hasRename) {
                    tempWatcher.Renamed += new RenamedEventHandler(OnRename);
                }
                if (hasDeletion) {
                    tempWatcher.Deleted += new FileSystemEventHandler(OnDeletion);
                }
                if (hasCreation) {
                    tempWatcher.Created += new FileSystemEventHandler(OnCreation);
                }
                AFSForm.QueueConsolePost(new ConsolePost("Modification watcher removed from " + this.watchableName, "", Color.Magenta, true));
            }
            this.watcher = tempWatcher;
            PostInitIntWatcher();
            Update();
        }

        /// <summary>
        /// Sets the stance of the watcher.
        /// </summary>
        /// <param name="parStance"></param>
        public void SetStance(EnumStance parStance) {
            this.stance = parStance;
            if (stance == EnumStance.Slient) {
                watcher.EnableRaisingEvents = false;
                AFSForm.QueueConsolePost(new ConsolePost("AFS: ", this.Name + "'s stance set to " + stance.ToString(), Color.OrangeRed, true));
            } else if (stance == EnumStance.Active) {
                watcher.EnableRaisingEvents = true;
                AFSForm.QueueConsolePost(new ConsolePost("AFS: ", this.Name + "'s stance set to " + stance.ToString(), Color.OrangeRed, true));
            } else if (stance == EnumStance.Passive) {
                watcher.EnableRaisingEvents = false;
                AFSForm.QueueConsolePost(new ConsolePost("AFS: ", this.Name + "'s stance set to " + stance.ToString(), Color.OrangeRed, true));
            }
            Update();
        }

        /// <summary>
        /// Prepares watcher and initates wait result thread.
        /// </summary>
        public void WaitEvent() {
            waitResultThread = new Thread(new ThreadStart(WaitResultCallByThread));
            waitResultThread.Name = "WatcherWaitThread";
            System.Timers.Timer waitTimer = new System.Timers.Timer(passiveWaitTime);
            waitTimer.Elapsed += waitTimer_Elapsed;
            waitResultThread.Start();
            waitTimer.Start();
        }

        /// <summary>
        /// Occurs when the wait time is complete, notifies watcher of threaded wait result(s(?)).
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Elapsed Event Arguments</param>
        void waitTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {
            if (threadedResult.ChangeType == WatcherChangeTypes.Created) {
                Notify(new Notification(Focus, EnumNotificationEventType.ObjectCreated));
            }
            if (threadedResult.ChangeType == WatcherChangeTypes.Deleted) {
                Notify(new Notification(Focus, EnumNotificationEventType.ObjectRemoved));
            }
            if (threadedResult.ChangeType == WatcherChangeTypes.Renamed) {
                Notify(new Notification(Focus, EnumNotificationEventType.ObjectRenamed));
            }
            if (threadedResult.ChangeType == WatcherChangeTypes.Changed) {
                Notify(new Notification(Focus, EnumNotificationEventType.ObjectChanged));
            }
            ((System.Timers.Timer)sender).Stop();
        }

        /// <summary>
        /// Calls for watcher wait for changed.
        /// </summary>
        private void WaitResultCallByThread() {
            threadedResult = watcher.WaitForChanged(WatcherChangeTypes.All, passiveWaitTime);
        }

        /// <summary>
        /// Gets the watcher stance.
        /// </summary>
        /// <returns>stance</returns>
        public EnumStance GetStance() {
            return stance;
        }

        /// <summary>
        /// Gets the last notification.
        /// </summary>
        /// <returns>latestNotification</returns>
        public Notification GetLastNotification() {
            return lastestNotification != null ? lastestNotification : sessionNotifications[notificationActiveIndex] != null ? sessionNotifications[notificationActiveIndex] : new Notification("ERROR: Notification at active index returns NULL", EnumNotificationEventType.CRIT_ERROR);
        }

        /// <summary>
        /// Gets the notifications array. 
        /// </summary>
        public Notification[] GetNotifications {
            get { return sessionNotifications; }
        }

        /// <summary>
        /// File/Directory OnCreation event.
        /// </summary>
        /// <param name="parSrc">Source</param>
        /// <param name="parEventArgs">File System Event Arguments</param>
        public void OnCreation(object parSrc, System.IO.FileSystemEventArgs parEventArgs) {
            Notify(new Notification(parEventArgs.Name, EnumNotificationEventType.ObjectCreated));
            AFSForm.OnCreation(parSrc, parEventArgs, this);
            Update();
        }

        /// <summary>
        /// File/Directory OnDeletion event.
        /// </summary>
        /// <param name="parSrc">Source</param>
        /// <param name="parEventArgs">File System Event Arguments</param>
        public void OnDeletion(object parSrc, System.IO.FileSystemEventArgs parEventArgs) {
            Notify(new Notification(parEventArgs.Name, EnumNotificationEventType.ObjectRemoved));
            AFSForm.OnDeletion(parSrc, parEventArgs, this);
            Update();
        }

        /// <summary>
        /// File/Directory OnChange event.
        /// </summary>
        /// <param name="parSrc">Source</param>
        /// <param name="parEventArgs">File System Event Arguments</param>
        public void OnChange(object parSrc, System.IO.FileSystemEventArgs parEventArgs) {
            Notify(new Notification(parEventArgs.Name, EnumNotificationEventType.ObjectChanged));
            AFSForm.OnChange(parSrc, parEventArgs, this);
            Update();
        }

        /// <summary>
        /// File/Directory OnRename event.
        /// </summary>
        /// <param name="parSrc">Source</param>
        /// <param name="parEventArgs">Renamed Event Arguments</param>
        public void OnRename(object parSrc, System.IO.RenamedEventArgs parEventArgs) {
            Notify(new Notification(parEventArgs.Name, EnumNotificationEventType.ObjectRenamed));
            AFSForm.OnRename(parSrc, parEventArgs, this);
            Update();
        }

        /// <summary>
        /// Adds a notification to the notification array.
        /// </summary>
        /// <param name="parNotification">Notification</param>
        public void Notify(Notification parNotification) {
            lastestNotification = parNotification;
            sessionNotifications[notificationActiveIndex] = lastestNotification;
            notificationActiveIndex++;
            Update();
        }

        /// <summary>
        /// Notifies the Omnitudal Observer that an update is required.
        /// </summary>
        private void Update() {
            OmnitudalObserver.needsUpdate = true;
        }

        /// <summary>
        /// Gets the watcher name.
        /// </summary>
        public String Name {
            get { return watchableName; }
        }

        /// <summary>
        /// Gets the watchers focus.
        /// </summary>
        public String Focus {
            get {return watchableFocus;}
        }

        /// <summary>
        /// Gets the watchers extent.
        /// </summary>
        public Boolean Extent {
            get {return watchSubDirs;}
        }

        /// <summary>
        /// Gets the notification count.
        /// Sets the notification count, should only be occured upon reading.
        /// </summary>
        public Int32 SessionNotificationCount {
            get {return notificationActiveIndex;}
            set { notificationActiveIndex = value; }
        }

        /// <summary>
        /// Gets and sets the Watcher Wait Timeout.
        /// </summary>
        public Int32 WatcherWaitTimeout {
            get { return passiveWaitTime; }
            set { passiveWaitTime = value; }
        }

        /// <summary>
        /// Gets the creation event status.
        /// </summary>
        public Boolean Creation {
            get { return hasCreation; }
        }

        /// <summary>
        /// Gets the deletion event status.
        /// </summary>
        public Boolean Deletion {
            get { return hasDeletion; }
        }

        /// <summary>
        /// Gets the rename event status.
        /// </summary>
        public Boolean Rename {
            get { return hasRename; }
        }

        /// <summary>
        /// Gets the change event status.
        /// </summary>
        public Boolean Change {
            get { return hasChange; }
        }

        /// <summary>
        /// Alternative to Name.
        /// </summary>
        /// <returns>watchableName</returns>
        public override string ToString() {            
            return watchableName;
        }
    }
}
