﻿namespace AllusionFileSystem
{
    partial class SetExtent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetExtent));
            this.radioButtonTopDown = new System.Windows.Forms.RadioButton();
            this.radioButtonDirectory = new System.Windows.Forms.RadioButton();
            this.buttonSet = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // radioButtonTopDown
            // 
            this.radioButtonTopDown.AutoSize = true;
            this.radioButtonTopDown.Location = new System.Drawing.Point(13, 13);
            this.radioButtonTopDown.Name = "radioButtonTopDown";
            this.radioButtonTopDown.Size = new System.Drawing.Size(75, 17);
            this.radioButtonTopDown.TabIndex = 0;
            this.radioButtonTopDown.TabStop = true;
            this.radioButtonTopDown.Text = "Top Down";
            this.radioButtonTopDown.UseVisualStyleBackColor = true;
            // 
            // radioButtonDirectory
            // 
            this.radioButtonDirectory.AutoSize = true;
            this.radioButtonDirectory.Location = new System.Drawing.Point(104, 13);
            this.radioButtonDirectory.Name = "radioButtonDirectory";
            this.radioButtonDirectory.Size = new System.Drawing.Size(91, 17);
            this.radioButtonDirectory.TabIndex = 0;
            this.radioButtonDirectory.TabStop = true;
            this.radioButtonDirectory.Text = "Directory Only";
            this.radioButtonDirectory.UseVisualStyleBackColor = true;
            // 
            // buttonSet
            // 
            this.buttonSet.Location = new System.Drawing.Point(201, 10);
            this.buttonSet.Name = "buttonSet";
            this.buttonSet.Size = new System.Drawing.Size(75, 23);
            this.buttonSet.TabIndex = 1;
            this.buttonSet.Text = "Set";
            this.buttonSet.UseVisualStyleBackColor = true;
            this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
            // 
            // SetExtent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 45);
            this.Controls.Add(this.buttonSet);
            this.Controls.Add(this.radioButtonDirectory);
            this.Controls.Add(this.radioButtonTopDown);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetExtent";
            this.Text = "Set Extent";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonTopDown;
        private System.Windows.Forms.RadioButton radioButtonDirectory;
        private System.Windows.Forms.Button buttonSet;
    }
}