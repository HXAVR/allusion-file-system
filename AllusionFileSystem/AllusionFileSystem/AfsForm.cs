﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Reflection;

namespace AllusionFileSystem
{
    public partial class AFSForm : Form
    {
        /// <summary>
        /// Determines whether the main system is running. If disabled during main operation application will cease to work.
        /// </summary>
        private Boolean runCore = false;
        /// <summary>
        /// Should the application use UTC times for timestamps.
        /// </summary>
        public static Boolean useUtc = false;
        /// <summary>
        /// Should the application post console watcher notifications.
        /// </summary>
        private Boolean postWatcherNotifications = false;
        /// <summary>
        /// Is the application in debug mode.
        /// </summary>
        private Boolean debugMode = false;

        /// <summary>
        /// Console delegate.
        /// </summary>
        /// <param name="post">Message to post</param>
        private delegate void richTextBoxDelegate(ConsolePost post);
        /// <summary>
        /// Generic delegate.
        /// </summary>
        private delegate void genericControlDelegate();

        /// <summary>
        /// List of threads started from main form.
        /// </summary>
        public static List<Thread> applicationThreads = new List<Thread>(8);
        /// <summary>
        /// Console output queue handler.
        /// </summary>
        private Thread ThreadQueueHandler;
        /// <summary>
        /// Command queue and parser handler.
        /// </summary>
        private Thread ThreadCommandHandler;
        /// <summary>
        /// Main application logic group.
        /// </summary>
        private Thread ThreadRunLoop; 

        /// <summary>
        /// Console output message queue.
        /// </summary>
        private static Queue<ConsolePost> outputQueue = new Queue<ConsolePost>(32);
        /// <summary>
        /// Command queue.
        /// </summary>
        private static Queue<String> commandQueue = new Queue<String>(32);

        /// <summary>
        /// Command atlis link dictionary.
        /// </summary>
        private Dictionary<String, Command> commandsMap = new Dictionary<String, Command>();

        /// <summary>
        /// Omnitudal Observer, handler of all watchers.
        /// </summary>
        private static OmnitudalObserver observer;

        /// <summary>
        /// Size of each watchers notification array.
        /// </summary>
        public const Int32 NOTIFICATION_SESSION_SIZE = 256;
        /// <summary>
        /// How often the main loop should run each second.
        /// </summary>
        public const Int32 tickFrequency = 10;

        /// <summary>
        /// Global command declaration and instantiation.
        /// </summary>
        private Command commandHelp = new  Command("Help", "RenderHelp", "help | ?", 0, 0);
        private Command commandShutdown = new Command("Stop", "Shutdown", "stop | exit", 0, 0);
        private Command commandAddWatcher = new Command("AddWatcher", "AddWatchable", "addWatcher {watcherName} {directory}", 1, 2);
        private Command commandRegister = new Command("Register", "RegisterWatcher", "register {watcherName} {creation|deletion|modification|rename}", 2, 2);
        private Command commandSetExtent = new Command("SetExtent", "SetWatcherExtent", "setExtent {watcherName} {true|false}", 2, 2);
        private Command commandClearInterface = new Command("Clear", "CleanInterface", "clear | reset | tidy", 0, 0);
        private Command commandSetNotifyAbility = new Command("PostNotifications", "SetNotificationPostStatus", "postNotifications {true|false}", 1, 1);
        private Command commandViewWatcher = new Command("View", "ViewWatcher", "view {watcherName}", 1, 1);
        private Command commandAwaitWatcher = new Command("Wait", "WaitWatcher", "wait {watcherName} {seconds}", 2, 2);
        private Command commandSetStance = new Command("SetStance", "StanceWatcher", "setStance {watcherName} {active|passive|silent}", 2, 2);
        private Command commandSetFocus = new Command("SetFocus", "SetFocus", "setFocus {watcherName} {focus}", 2, 2);
        private Command commandDeleteRegister = new Command("DeleteRegister", "DeleteRegister", "deleteRegister {watcherName} {creation|deleton|modification|rename}", 2, 2);
        private Command commandDeleteWatcher = new Command("DeleteWatcher", "DeleteWatcher", "deleteWatcher {watcherName}", 1, 1);
        private Command commandToggleDebugMode = new Command("ToggleDebugMode", "UpdateDebugMode", "toggleDebugMode", 0, 0);
        private Command commandDebugWatcher = new Command("DebugWatcher", "DebugWatcher", "debugWatcher [optional{registers=0|1|2|3|4}]", 1, 1).SetDebugCommand();
        private Command commandDebugRestart = new Command("Restart", "RestartApp", "restart", 0, 0).SetDebugCommand();
        private Command commandDebugEcho = new Command("Echo", "DebugEcho", "echo {message}", 1, 1).SetDebugCommand();
        
        /// <summary>
        /// Form constructor
        /// </summary>
        public AFSForm() {
            InitializeComponent();
        }

        /// <summary>
        /// Sends a message to the console.
        /// </summary>
        /// <param name="post">Message to post</param>
        private void PostToConsole(ConsolePost post) {
            BeginInvoke(new richTextBoxDelegate(HandelPost), post);
        }

        /// <summary>
        /// Prints post to the console.
        /// </summary>
        /// <param name="post">Message to print</param>
        private void HandelPost(ConsolePost post) {
            if (post != null) {
                consoleRichTextBox.SelectionStart = consoleRichTextBox.Text.Length;
                consoleRichTextBox.ScrollToCaret();
                if (post.NewLine) {
                    consoleRichTextBox.SelectedText += "\n";
                } else {
                    consoleRichTextBox.SelectionColor = post.HeaderColor;
                    consoleRichTextBox.SelectedText += post.PostOwner;
                    consoleRichTextBox.SelectionColor = Color.White;
                    consoleRichTextBox.SelectedText += post.Message;
                    if (post.FollowWithNewLine) {
                        consoleRichTextBox.SelectedText += "\n";
                    }
                    consoleRichTextBox.SelectionStart = consoleRichTextBox.Text.Length;
                    consoleRichTextBox.ScrollToCaret();
                }
            }
        }

        #region CommandMethods

        /// <summary>
        /// Deletes a event from a specified watcher.
        /// </summary>
        /// <param name="args">[0] = watcherName [1] = creation|deletion|modification|rename</param>
        public virtual void DeleteRegister(Object[] args) {
            Watchable focusedWatchable = observer.GetWatchable((String)args[0]);
            if (focusedWatchable != null) {
                switch (((String)args[1]).ToLower()) {
                    case "creation":
                        focusedWatchable.RemoveEvent(EnumNotificationEventType.ObjectCreated);
                        break;
                    case "deletion":
                        focusedWatchable.RemoveEvent(EnumNotificationEventType.ObjectRemoved);
                        break;
                    case "modification":
                        focusedWatchable.RemoveEvent(EnumNotificationEventType.ObjectChanged);
                        break;
                    case "renamed":
                        focusedWatchable.RemoveEvent(EnumNotificationEventType.ObjectRenamed);
                        break;
                    default:
                        AFSForm.QueueConsolePost(new ConsolePost("AFS: ", "Incorrect usage! Use " + commandsMap["deleteregister"].GetUsage(), Color.OrangeRed, true));
                        break;
                }
            }
        }

        /// <summary>
        /// UNUSED Closes application. Leftover from original console base.
        /// </summary>
        /// <param name="args">Shutdown arguments</param>
        public virtual void Shutdown(Object[] args) {
            /*
            this.runCore = false;
            if (args.Length > 0 && args[0] != null) {
                if ((String)args[0] == "true") {
                    killedSelf = true;
                }
            }*/
        }

        /// <summary>
        /// Adds a watchable to the observer.
        /// </summary>
        /// <param name="args">[0] = Name, [1] = Focus</param>
        public virtual void AddWatchable(Object[] args) {
            if (args.Length >= commandsMap["addwatcher"].GetMinParameterCount()) {
                if (args.Length == 2) {
                    if (Directory.Exists((String)args[1])) {
                        observer.AddWatchable(new Watchable((String)args[0], (String)args[1]));
                    } else {
                        AFSForm.QueueConsolePost(new ConsolePost("AFS: ", "Directory does not exist! Watcher creation aborted!", Color.OrangeRed, true));
                    }
                } else if (args.Length == 1) {
                    observer.AddWatchable(new Watchable((String)args[0]));
                }
            }
        }

        /// <summary>
        /// Registers an event to a specified watcher.
        /// </summary>
        /// <param name="args">[0] = Watcher name, [1] = True | False</param>
        public virtual void RegisterWatcher(Object[] args) {
            Watchable focusedWatchable = observer.GetWatchable((String)args[0]);
            if (focusedWatchable != null) {
                switch ((String)args[1]) {
                    case "creation":
                        focusedWatchable.AddEvent(EnumNotificationEventType.ObjectCreated);
                        break;
                    case "deletion":
                        focusedWatchable.AddEvent(EnumNotificationEventType.ObjectRemoved);
                        break;
                    case "modification":
                        focusedWatchable.AddEvent(EnumNotificationEventType.ObjectChanged);
                        break;
                    case "rename":
                        focusedWatchable.AddEvent(EnumNotificationEventType.ObjectRenamed);
                        break;
                    default:
                        AFSForm.QueueConsolePost(new ConsolePost("AFS: ", "Incorrect usage! Use " + commandsMap["register"].GetUsage(), Color.OrangeRed, true));
                        break;
                }
            }
        }

        /// <summary>
        /// Sets the extent that the watcher watches.
        /// </summary>
        /// <param name="args">[0] = Watcher name, [1] = Extent</param>
        public virtual void SetWatcherExtent(Object[] args) {
            Watchable focusedWatchable = observer.GetWatchable((String)args[0]);
            if (focusedWatchable != null) {
                Boolean val = Boolean.TryParse((String)args[1], out val);
                focusedWatchable.SetExtent(val);
                String vwe = focusedWatchable.Extent ? "Top Down" : "Directory Only";
                AFSForm.QueueConsolePost(new ConsolePost("AFS: ", focusedWatchable.Name + " extent set to: " + vwe, Color.OrangeRed, true));
            }

        }
        
        /// <summary>
        /// Renders help for all commands, showing command and usage.
        /// </summary>
        /// <param name="args">Arguments</param>
        public virtual void RenderHelp(Object[] args) {
            for (Int32 i = 0; i < commandsMap.Count; i++) {
                Command cmd = commandsMap.ElementAt(i).Value;
                AFSForm.QueueConsolePost(new ConsolePost(cmd.GetCommandName() + " : ", "", Color.Cyan, false));
                AFSForm.QueueConsolePost(new ConsolePost("", cmd.GetUsage(), Color.White, true));
            }
        }

        /// <summary>
        /// Clears the console output.
        /// </summary>
        /// <param name="args">Arguments</param>
        public virtual void CleanInterface(Object[] args) {
            this.BeginInvoke(new genericControlDelegate(consoleRichTextBox.Clear));
            AFSForm.QueueConsolePost(new ConsolePost("AFS: ", "Session run details.", Color.OrangeRed, true));
        }

        /// <summary>
        /// Sets the application's notification status. True if to post notifications to the console, False if to remain silent. Defaults to false.
        /// </summary>
        /// <param name="args">[0] = True | False</param>
        public virtual void SetNotificationPostStatus(Object[] args) {
            this.postWatcherNotifications = !this.postWatcherNotifications;
            AFSForm.QueueConsolePost(new ConsolePost("AFS: ", "Post Notifications set to " + postWatcherNotifications, Color.OrangeRed, true));
        }
        
        /// <summary>
        /// Prints information regarding a specific watcher.
        /// </summary>
        /// <param name="args">[0] = Watcher name</param>
        public virtual void ViewWatcher(Object[] args) {
            Watchable focusedWatchable = observer.GetWatchable((String)args[0]);
            if (focusedWatchable != null) {
                AFSForm.QueueConsolePost(new ConsolePost("Frontend name", focusedWatchable.Name, Color.Gray, true));
                AFSForm.QueueConsolePost(new ConsolePost("Directory", focusedWatchable.Focus, Color.Gray, true));
                AFSForm.QueueConsolePost(new ConsolePost("Current stance", focusedWatchable.GetStance().ToString(), Color.Gray, true));
                String vwe = focusedWatchable.Extent ? "Top Down" : "Directory Only";
                AFSForm.QueueConsolePost(new ConsolePost("View extent", vwe, Color.Gray, true));
                AFSForm.QueueConsolePost(new ConsolePost("Notification count", focusedWatchable.SessionNotificationCount.ToString(), Color.Gray, true));
            }
        }

        /// <summary>
        /// Waits for a change in the specified watcher's focus for a set period of time.
        /// </summary>
        /// <param name="args">[0] = Watcher name, [1] = Wait time (milliseconds)</param>
        public virtual void WaitWatcher(Object[] args) {
            Watchable focusedWatchable = observer.GetWatchable((String)args[0]);
            if (focusedWatchable != null) {
                Int32 seconds;
                Int32.TryParse((String)args[1], out seconds);
                focusedWatchable.WatcherWaitTimeout = seconds * 1000;
                focusedWatchable.WaitEvent();
            }
        }

        /// <summary>
        /// Sets the stance of a watcher.
        /// </summary>
        /// <param name="args">[0] = Watcher name, [1] = Stance {active | passive | silent}</param>
        public virtual void StanceWatcher(Object[] args) {
            Watchable focusedWatchable = observer.GetWatchable((String)args[0]);
            if (focusedWatchable != null) {
                switch (((String)args[1]).ToLower()) {
                    case "active":
                        focusedWatchable.SetStance(EnumStance.Active);
                        break;
                    case "passive":
                        focusedWatchable.SetStance(EnumStance.Passive);
                        break;
                    case "silent":
                        focusedWatchable.SetStance(EnumStance.Slient);
                        break;
                    default:
                        AFSForm.QueueConsolePost(new ConsolePost("AFS: ", "No such stance! Recieved: " + ((String)args[1]).ToLower(), Color.OrangeRed, true));
                        break;
                }
            }
        }

        /// <summary>
        /// Sets the specified watchers focus.
        /// </summary>
        /// <param name="args">Arguments</param>
        public virtual void SetFocus(Object[] args) {
            Watchable focusedWatchable = observer.GetWatchable((String)args[0]);
            if (focusedWatchable != null) {
                if (Directory.Exists((String)args[1])) {
                    focusedWatchable.SetFocus((String)args[1]);
                }
            }
        }

        /// <summary>
        /// Deletes the specified watcher.
        /// </summary>
        /// <param name="args">Arguments</param>
        public virtual void DeleteWatcher(Object[] args) {
            Int32 index = observer.GetWatchableIndex((String)args[0]);
            if (index >= 0) {
                observer.RemoveWatchable(index);
            }
        }

        /// <summary>
        /// Toggles the debug mode state.
        /// </summary>
        /// <param name="args">Arguments</param>
        public virtual void UpdateDebugMode(Object[] args) {
            debugMode = !debugMode;
            AFSForm.QueueConsolePost(new ConsolePost("AFS: ", "Debug mode set to " + debugMode, Color.OrangeRed, true));
        }

        #region debugCommands

        /// <summary>
        /// Debug command. Echos user input.
        /// </summary>
        /// <param name="args">[0] = user input</param>
        public virtual void DebugEcho(Object[] args) {
            if (args.Length > 0) {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < args.Length; i++){
                    builder.Append((String)args[i]).Append(" ");
                }
                AFSForm.QueueConsolePost(new ConsolePost("Echo: ", builder.ToString(), Color.Purple, true));
            }
        }

        /// <summary>
        /// Debug command. Adds a watcher to the observer, and registers any specified Events.
        /// </summary>
        /// <param name="args">[0] = Event(s) to add.</param>
        public virtual void DebugWatcher(Object[] args) {
            AddWatchable(new String[] { "DebugWatcher", "C:/Users/Michele/Desktop" });
            if (args.Length > 0) {
                switch ((String)args[0]) {
                    case "0":
                        RegisterWatcher(new String[] { "DebugWatcher", "creation" });
                        break;
                    case "1":
                        RegisterWatcher(new String[] { "DebugWatcher", "deletion" });
                        break;
                    case "2":
                        RegisterWatcher(new String[] { "DebugWatcher", "modification" });
                        break;
                    case "3":
                        RegisterWatcher(new String[] { "DebugWatcher", "rename" });
                        break;
                    case "4":
                        RegisterWatcher(new String[] { "DebugWatcher", "creation" });
                        RegisterWatcher(new String[] { "DebugWatcher", "deletion" });
                        RegisterWatcher(new String[] { "DebugWatcher", "modification" });
                        RegisterWatcher(new String[] { "DebugWatcher", "rename" });
                        break;
                }
            }
        }

        /// <summary>
        /// UNUSED Restarts the application. Leftover from console base.
        /// </summary>
        /// <param name="args">Arguments</param>
        public virtual void RestartApp(Object[] args) {
            Process.Start("AFS.exe");
            this.Shutdown(new String[] { "true" });
        }

        #endregion
        #endregion

        /// <summary>
        /// Occurs when the form loads. Initilises core objects and threads.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event Arguments</param>
        private void AFSForm_Load(object sender, EventArgs e) {
            observer = new OmnitudalObserver(256, this);
            MapCommands();

            runCore = true;
            ThreadRunLoop = new Thread(new ThreadStart(RunApplicationLoop));
            ThreadRunLoop.Name = "ApplicationLoopThread";
            applicationThreads.Add(ThreadRunLoop);
            ThreadRunLoop.Start();
            ThreadQueueHandler = new Thread(new ThreadStart(RunConsoleOutput));
            ThreadQueueHandler.Name = "ConsoleOutputThread";
            applicationThreads.Add(ThreadQueueHandler);
            ThreadQueueHandler.Start();
            ThreadCommandHandler = new Thread(new ThreadStart(RunCommandParser));
            ThreadCommandHandler.Name = "CommandHandlerThread";
            applicationThreads.Add(ThreadCommandHandler);
            ThreadCommandHandler.Start();
            ReadFiles();
        }

        /// <summary>
        /// Occurs when a key is pressed in the console input. Handles input history and posting commands.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Key Event Arguments</param>
        private void consoleTextBox_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                if (!String.IsNullOrWhiteSpace(consoleTextBox.Text)) {
                    consoleTextBox.AutoCompleteCustomSource.Add(consoleTextBox.Text);
                    AFSForm.QueueCommand(((TextBox)sender).Text);
                    AFSForm.QueueConsolePost(new ConsolePost(((TextBox)sender).Text, "", Color.Lime, true));
                    consoleTextBox.Clear();
                }
            }
        }

        /// <summary>
        /// Gets application relative time.
        /// </summary>
        /// <returns>Time in UTC format, if specified, else local time.</returns>
        public static DateTime GetRelativeTime() {
            if (AFSForm.useUtc) {
                return DateTime.UtcNow;
            } else {
                return DateTime.Now;
            }
        }

        /// <summary>
        /// Adds a console post to the output queue.
        /// </summary>
        /// <param name="post">Post to queue</param>
        public static void QueueConsolePost(ConsolePost post) {
            outputQueue.Enqueue(post);
        }

        /// <summary>
        /// Adds a command to the command queue.
        /// </summary>
        /// <param name="cmd">Command</param>
        public static void QueueCommand(String cmd) {
            commandQueue.Enqueue(cmd);
        }

        /// <summary>
        /// Application inturnal logic loop. Must be called by a non-ui thread due to blocking.
        /// </summary>
        public void RunApplicationLoop() {
            while (runCore) {
                try {
                    observer.SortWatchables();
                    Thread.Sleep(1000 / AFSForm.tickFrequency);
                } catch (Exception ex) {
                    if (debugMode) {
                        AFSForm.QueueConsolePost(new ConsolePost("!!!EXCEPTION IN CORE RUN!!! ", DateTime.Now.ToShortTimeString(), Color.Red, true));
                        StringBuilder builder = new StringBuilder().Append("Exceptions: ").Append(ex.ToString());
                        AFSForm.QueueConsolePost(new ConsolePost("AFS: ", builder.ToString(), Color.OrangeRed, true));
                    }
                } finally {
                    GC.Collect(2);
                }
            }
            foreach (Thread t in applicationThreads) {
                if (t.ThreadState != System.Threading.ThreadState.Aborted) {
                    t.Abort();
                }
            }
            Int64 gcPre = GC.GetTotalMemory(false);
            GC.Collect();
            Int64 gcPost = GC.GetTotalMemory(false);
            Int64 gcDiff = gcPre - gcPost;
            AFSForm.QueueConsolePost(new ConsolePost("GC_TOTAL_MEM_PRIOR_FORCED_COLLECT: " + gcPre, "", Color.LightGoldenrodYellow, false));
            AFSForm.QueueConsolePost(new ConsolePost("GC_TOTAL_MEM_AFTER_FORCED_COLLECT: " + gcPost, "", Color.LightGoldenrodYellow, false));
            AFSForm.QueueConsolePost(new ConsolePost("GC_DIFF: " + gcDiff + " bytes", "", Color.LightGoldenrodYellow, false));
            Thread.Sleep(1400);
            AFSForm.QueueConsolePost(new ConsolePost("Shutdown complete!", "", Color.LightGoldenrodYellow, false));
        }

        /// <summary>
        /// Handles dequeuing of console post messages to the console. Must be threaded.
        /// </summary>
        public void RunConsoleOutput() {
            while (runCore) {
                while (AFSForm.outputQueue.Count > 0) {
                 PostToConsole(AFSForm.outputQueue.Dequeue());
                }
            }
        }

        /// <summary>
        /// Handles dequeuing and parsing of console commands.
        /// </summary>
        private void RunCommandParser() {
            while (runCore) {
                while (AFSForm.commandQueue.Count > 0) {
                    String input = AFSForm.commandQueue.Dequeue();
                    if (input.Length > 0) {
                        String[] command = input.Split(null);
                        Boolean invoke = true;
                        Type objType = this.GetType();
                        try {
                            if (commandsMap.ContainsKey(command[0])) {
                                Command focusedCommand = commandsMap[command[0]];
                                MethodInfo methodInfo = objType.GetMethod(focusedCommand.GetMethodName());
                                Object[] parameters = null;
                                if (focusedCommand.IsDebugCommand()) {
                                    if (!debugMode) {
                                        invoke = false;   
                                    }
                                }
                                if (invoke) {
                                    if (command.Length >= focusedCommand.GetMinParameterCount()) {
                                        parameters = new Object[command.Length - 1];
                                        for (Int32 i = 0; i < command.Length - 1; i++) {
                                            parameters[i] = command[i + 1];
                                        }
                                    } else {
                                        AFSForm.QueueConsolePost(new ConsolePost("Incorrect usage! Use " + focusedCommand.GetUsage(), "", Color.Red, true));
                                        invoke = false;
                                    }
                                }
                                if (invoke) {
                                    Object[] paramFinal = new Object[] { parameters };
                                    methodInfo.Invoke(this, paramFinal);
                                }
                            }
                        } catch (KeyNotFoundException knfex) {
                            AFSForm.QueueConsolePost(new ConsolePost("AFS: ", "Not a valid command!", Color.OrangeRed, true));
                        } catch (Exception ex) {    }
                    }

                }
            }
        }

        /// <summary>
        /// Maps application commands and atlises.
        /// </summary>
        private void MapCommands() {
            commandsMap.Add("exit", commandShutdown);
            commandsMap.Add("stop", commandShutdown);
            commandsMap.Add("addwatcher", commandAddWatcher);
            commandsMap.Add("register", commandRegister);
            commandsMap.Add("setextent", commandSetExtent);
            commandsMap.Add("help", commandHelp);
            commandsMap.Add("?", commandHelp);
            commandsMap.Add("clear", commandClearInterface);
            commandsMap.Add("reset", commandClearInterface);
            commandsMap.Add("tidy", commandClearInterface);
            commandsMap.Add("postnotifications", commandSetNotifyAbility);
            commandsMap.Add("notify", commandSetNotifyAbility);
            commandsMap.Add("view", commandViewWatcher);
            commandsMap.Add("wait", commandAwaitWatcher);
            commandsMap.Add("setstance", commandSetStance);
            commandsMap.Add("setfocus", commandSetFocus);
            commandsMap.Add("deleteevent", commandDeleteRegister);
            commandsMap.Add("removeevent", commandDeleteRegister);
            commandsMap.Add("unregister", commandDeleteRegister);
            commandsMap.Add("deletewatcher", commandDebugWatcher);
            commandsMap.Add("toggleDebugMode", commandToggleDebugMode);
            commandsMap.Add("restart", commandDebugRestart);
            commandsMap.Add("debugwatcher", commandDebugWatcher);
            commandsMap.Add("dwatcher", commandDebugWatcher);
            commandsMap.Add("echo", commandDebugEcho);

            for (int i = 0; i < commandsMap.Count; i++) {
                if (!commandsMap.ElementAt(i).Value.IsDebugCommand()) {
                    consoleTextBox.AutoCompleteCustomSource.Add(commandsMap.ElementAt(i).Key);
                }
            }
        }

        /// <summary>
        /// Global OnCreation event.
        /// </summary>
        /// <param name="parSrc">Source</param>
        /// <param name="parEventArgs">File System Event Arguments</param>
        /// <param name="parWatchable">Watchable</param>
        public static void OnCreation(object parSrc, System.IO.FileSystemEventArgs parEventArgs, Watchable parWatchable) {
            AFSForm.QueueConsolePost(new ConsolePost(parWatchable.GetLastNotification().GetNotification(), "", Color.White, true));
        }

        /// <summary>
        /// Global OnDeletion event.
        /// </summary>
        /// <param name="parSrc">Source</param>
        /// <param name="parEventArgs">File System Event Arguments</param>
        /// <param name="parWatchable">Watchable</param>
        public static void OnDeletion(object parSrc, System.IO.FileSystemEventArgs parEventArgs, Watchable parWatchable) {
            AFSForm.QueueConsolePost(new ConsolePost(parWatchable.GetLastNotification().GetNotification(), "", Color.White, true));
        }

        /// <summary>
        /// Global OnChange event.
        /// </summary>
        /// <param name="parSrc">Source</param>
        /// <param name="parEventArgs">File System Event Arguments</param>
        /// <param name="parWatchable">Watchable</param>
        public static void OnChange(object parSrc, System.IO.FileSystemEventArgs parEventArgs, Watchable parWatchable) {
            AFSForm.QueueConsolePost(new ConsolePost(parWatchable.GetLastNotification().GetNotification(), "", Color.White, true));
        }

        /// <summary>
        /// Global OnRename event.
        /// </summary>
        /// <param name="parSrc">Source</param>
        /// <param name="parEventArgs">File System Event Arguments</param>
        /// <param name="parWatchable">Watchable</param>
        public static void OnRename(object parSrc, System.IO.RenamedEventArgs parEventArgs, Watchable parWatchable) {
            AFSForm.QueueConsolePost(new ConsolePost(parWatchable.GetLastNotification().GetNotification(), "", Color.White, true));
        }

        /// <summary>
        /// Occurs when the form starts closing.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Form Closing Event Arguments</param>
        private void AFSForm_FormClosing(object sender, FormClosingEventArgs e) {
            runCore = false;
            Save();
            foreach (Thread t in applicationThreads) {
                if (t.ThreadState != System.Threading.ThreadState.Aborted) {
                    t.Abort();
                }
            }    
        }

        /// <summary>
        /// Handles DataGridView cell clicks as well as displays any notifications relevent to the clicked watcher in the list box.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Data Grid View Cell Event Arguments</param>
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e) {
            Int32 ri = e.RowIndex;
            if (ri >= 0 & ri <= 256) {
                Watchable focusedWatchable = observer.WatchableArray[ri];
                listBoxNotifications.Items.Clear();
                foreach (Notification n in focusedWatchable.GetNotifications) {
                    if (n != null) {
                        listBoxNotifications.Items.Add(n.GetNotification());
                    }
                }
            }
        }

        /// <summary>
        /// Saves all application settings and watchers.
        /// </summary>
        private void Save() {
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "/settings.txt")) {
                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "/settings.txt");

            }
            using (StreamWriter writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "/settings.txt")){
                writer.WriteLine("UTC=" + useUtc);
                writer.WriteLine("POSTWATCHERNOTIFICATIONS=" + postWatcherNotifications);
                writer.WriteLine("DEBUGMODE=" + debugMode);
                writer.WriteLine("NOTIFICATIONSESSIONSIZECONSTANT=" + NOTIFICATION_SESSION_SIZE);
                writer.WriteLine("TICKFREQUENCYCONSTANT=" + tickFrequency);
            }

            if (File.Exists((AppDomain.CurrentDomain.BaseDirectory + "/watchers.txt"))) {
                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "/watchers.txt");
                using (StreamWriter writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "watchers.txt")) {
                    foreach (Watchable w in observer.WatchableArray) {
                        if (w != null) {
                            writer.WriteLine("VOID");
                            writer.WriteLine("NAME=" + w.Name);
                            writer.WriteLine("FOCUS=" + w.Focus);
                            writer.WriteLine("EXTENT=" + w.Extent);
                            writer.WriteLine("STANCE=" + w.GetStance());
                            writer.WriteLine("CREATION=" + w.Creation);
                            writer.WriteLine("DELETION=" + w.Deletion);
                            writer.WriteLine("RENAME=" + w.Rename);
                            writer.WriteLine("CHANGE=" + w.Change);
                            writer.WriteLine("BEGNOTIFICATIONS");
                            foreach (Notification n in w.GetNotifications) {
                                if (n != null) {
                                    writer.WriteLine("NNF");
                                    writer.WriteLine(n.GetEventType());
                                    writer.WriteLine(n.GetFocus());
                                }
                            }
                            writer.WriteLine("ENDNOTIFICATIONS");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Reads application settings and watchers.
        /// </summary>
        private void ReadFiles() {
            String input;
            Boolean runNotifyLoop = false;
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "/settings.txt")) {
                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "/settings.txt");
                using (StreamReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "/settings.txt")){
                    Boolean.TryParse(reader.ReadLine().Remove(0, 4), out useUtc);
                    Boolean.TryParse(reader.ReadLine().Remove(0, 25), out postWatcherNotifications);
                    Boolean.TryParse(reader.ReadLine().Remove(0, 10), out debugMode);
                }
            }
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "/watchers.txt")) {
                using (StreamReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "/watchers.txt")) {
                    Int32 watcherCount = 0;
                    while (!reader.EndOfStream) {
                        input = reader.ReadLine();
                        if (input == "VOID") {
                            String[] watcherBase = new String[8];
                            watcherBase[0] = reader.ReadLine().Remove(0, 5);
                            watcherBase[1] = reader.ReadLine().Remove(0, 6);
                            watcherBase[2] = reader.ReadLine().Remove(0, 7);
                            watcherBase[3] = reader.ReadLine().Remove(0, 7);
                            watcherBase[4] = reader.ReadLine().Remove(0, 9);
                            watcherBase[5] = reader.ReadLine().Remove(0, 9);
                            watcherBase[6] = reader.ReadLine().Remove(0, 7);
                            watcherBase[7] = reader.ReadLine().Remove(0, 7);
                            Watchable newWatchable = new Watchable(watcherBase[0], watcherBase[1]);
                            Boolean generic;
                            Boolean.TryParse(watcherBase[2], out generic);
                            newWatchable.SetExtent(generic);
                            switch (watcherBase[3]) {
                                case "active":
                                    newWatchable.SetStance(EnumStance.Active);
                                    break;
                                case "passive":
                                    newWatchable.SetStance(EnumStance.Passive);
                                    break;
                                case "silent":
                                    newWatchable.SetStance(EnumStance.Slient);
                                    break;
                                default:
                                    newWatchable.SetStance(EnumStance.Slient);
                                    break;
                            }
                            Boolean.TryParse(watcherBase[4], out generic);
                            if (generic) {
                                newWatchable.AddEvent(EnumNotificationEventType.ObjectCreated);
                            }
                            Boolean.TryParse(watcherBase[5], out generic);
                            if (generic) {
                                newWatchable.AddEvent(EnumNotificationEventType.ObjectRemoved);
                            }
                            Boolean.TryParse(watcherBase[6], out generic);
                            if (generic) {
                                newWatchable.AddEvent(EnumNotificationEventType.ObjectRenamed);
                            }
                            Boolean.TryParse(watcherBase[7], out generic);
                            if (generic) {
                                newWatchable.AddEvent(EnumNotificationEventType.ObjectChanged);
                            }
                            if (reader.ReadLine() == "BEGNOTIFICATIONS") {
                                runNotifyLoop = true;
                            }
                            while (runNotifyLoop) {
                                Int32 notificationCount = 0;
                                Notification temp;
                                String name;
                                EnumNotificationEventType type;
                                String tempStr = reader.ReadLine();
                                if (tempStr != "ENDNOTIFICATIONS" & tempStr == "NNF") {
                                    name = reader.ReadLine();
                                    switch (reader.ReadLine()) {
                                        case "ObjectCreated":
                                            type = EnumNotificationEventType.ObjectChanged;
                                            break;
                                        case "ObjectRemoved":
                                            type = EnumNotificationEventType.ObjectRemoved;
                                            break;
                                        case "ObjectChanged":
                                            type = EnumNotificationEventType.ObjectChanged;
                                            break;
                                        case "ObjectRenamed":
                                            type = EnumNotificationEventType.ObjectRenamed;
                                            break;
                                        case "ApplicationForced":
                                            type = EnumNotificationEventType.ApplicationForced;
                                            break;
                                        case "NewWatchableFocus":
                                            type = EnumNotificationEventType.NewWatchableFocus;
                                            break;
                                        case "CRIT_ERROR":
                                            type = EnumNotificationEventType.CRIT_ERROR;
                                            break;
                                        default:
                                            type = EnumNotificationEventType.CRIT_ERROR;
                                            break;
                                    }
                                    temp = new Notification(name, type);
                                    newWatchable.Notify(temp);
                                    notificationCount++;
                                }
                                if (tempStr == "ENDNOTIFICATIONS") {
                                    runNotifyLoop = false;
                                    newWatchable.SessionNotificationCount = notificationCount;
                                    observer.AddWatchable(newWatchable);
                                    watcherCount++;
                                }
                            }
                        }
                    }
                    observer.WatchableActiveIndex = watcherCount;
                }
            }
        }

        /// <summary>
        /// Opens New Watcher form as a dialog.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event Arguments</param>
        private void btnNewWatcher_Click(object sender, EventArgs e) {
            New_Watcher nwWindow = new New_Watcher(this);
            nwWindow.ShowDialog();
        }

        /// <summary>
        /// Opens Set Stance form as a dialog.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event Arguments</param>
        private void btnSetState_Click(object sender, EventArgs e) {
            Int32 selectedIndex = dataGridView1.CurrentCell.RowIndex;
            if (selectedIndex >= 0) {
                SetStance stWindow = new SetStance(observer.WatchableArray[selectedIndex].Name);
                stWindow.ShowDialog();
            }
        }

        /// <summary>
        /// Opens Set Extent form as a dialog.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event Args</param>
        private void btnSetExtent_Click(object sender, EventArgs e) {
            Int32 selectedIndex = dataGridView1.CurrentCell.RowIndex;
            if (selectedIndex >= 0) {
                SetExtent seWindow = new SetExtent(observer.WatchableArray[selectedIndex].Name);
                seWindow.ShowDialog();
            }
        }

        /// <summary>
        /// Opens Set Focus form as a dialog.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event Arguments</param>
        private void btnSetFocus_Click(object sender, EventArgs e) {
            Int32 selectedIndex = dataGridView1.CurrentCell.RowIndex;
            if (selectedIndex >= 0) {
                SetFocus sfWindow = new SetFocus(observer.WatchableArray[selectedIndex].Name);
                sfWindow.ShowDialog();
            }
        }

        /// <summary>
        /// Opens Register Event form as a dialog.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event Args</param>
        private void btnRegisterEvent_Click(object sender, EventArgs e) {
            Int32 selectedIndex = dataGridView1.CurrentCell.RowIndex;
            if (selectedIndex >= 0) {
                RegisterWatcher rwWindow = new RegisterWatcher(observer.WatchableArray[selectedIndex].Name);
                rwWindow.ShowDialog();
            }
        }
    }
}
