﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllusionFileSystem
{
    public class Notification
    {
        /// <summary>
        /// Notification time stamp.
        /// </summary>
        private DateTime timeStamp;
        /// <summary>
        /// Notification event type.
        /// </summary>
        private EnumNotificationEventType notificationEventType;
        /// <summary>
        /// Notification event location.
        /// </summary>
        private String focusable;

        /// <summary>
        /// Instantiates a new Notification.
        /// </summary>
        /// <param name="parFocused">Event location</param>
        /// <param name="parEventType">Notification Event Type</param>
        public Notification(String parFocused, EnumNotificationEventType parEventType) {
            this.focusable = parFocused;
            this.notificationEventType = parEventType;
            timeStamp = AFSForm.GetRelativeTime();
        }

        /// <summary>
        /// Gets a formatted notification message.
        /// </summary>
        /// <returns>Formatted message</returns>
        public String GetNotification() {
            String returnable = "A " + notificationEventType.ToString() + " event occured at " + timeStamp + " with the " + focusable + ".";
            return returnable;
        }

        /// <summary>
        /// Gets the notification event location.
        /// </summary>
        /// <returns>focusable</returns>
        public String GetFocus() {
            return focusable;
        }

        public EnumNotificationEventType GetEventType() {
            return notificationEventType;
        }

        /// <summary>
        /// Gets the event time stamp.
        /// </summary>
        /// <returns>timeStamp</returns>
        public DateTime GetTimeStamp() {
            return timeStamp;
        }
    }
}
