﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;

namespace AllusionFileSystem
{
    public class OmnitudalObserver
    {
        /// <summary>
        /// Instance of the Allusion File System Instance.
        /// </summary>
        private AFSForm frmInstance;
        /// <summary>
        /// Application watchable array.
        /// </summary>
        private Watchable[] watchables;
        /// <summary>
        /// Dynamic list of active watchables.
        /// </summary>
        private List<Watchable> activeWatchables = new List<Watchable>();
        /// <summary>
        /// Dynamic list of passive watchables.
        /// </summary>
        private List<Watchable> passiveWatchables = new List<Watchable>();
        /// <summary>
        /// Current position in the watchable array.
        /// </summary>
        private Int32 watchableActiveIndex = 0;
        /// <summary>
        /// Determines whether the observer needs to conduct an update.
        /// </summary>
        public static Boolean needsUpdate = false;
        /// <summary>
        /// Label update delegate.
        /// </summary>
        /// <param name="lbl">Label</param>
        /// <param name="val">Value</param>
        private delegate void UpdateLabelDelegate(Label lbl, String val);
        /// <summary>
        /// Data Grid View Update Delegate.
        /// </summary>
        /// <param name="watch">Watchable</param>
        private delegate void UpdateDGVDelegate(Watchable watch);

        /// <summary>
        /// Instantiates a new OmnitudalObserver.
        /// </summary>
        /// <param name="parSize">Watchables array size</param>
        /// <param name="parInstance">Allusion File System Instance</param>
        public OmnitudalObserver(Int32 parSize, AFSForm parInstance) {
            watchables = new Watchable[parSize];
            frmInstance = parInstance;
        }

        /// <summary>
        /// Gets the Watchable array.
        /// </summary>
        public Watchable[] WatchableArray {
            get { return watchables; }
        }

        /// <summary>
        /// Gets the Watchable array in the form of a dynamic list.
        /// </summary>
        public List<Watchable> WatchableList {
            get {
                List<Watchable> temp = new List<Watchable>();
                for (int i = 0; i < watchables.Length; i++) {
                    temp.Add(watchables[i]);
                }
                return temp;
            }
        }

        /// <summary>
        /// Sorts watchables and invokes label and data grid view updates.
        /// </summary>
        public void SortWatchables() {
            if (needsUpdate) {
                frmInstance.dataGridView1.Rows.Clear();
            }
            for (Int32 i = 0; i < watchables.Length; i++) {
                Watchable focusedWatchable = watchables[i];
                if (focusedWatchable != null) {
                    switch (focusedWatchable.GetStance()) {
                        case EnumStance.Active:
                            if (!activeWatchables.Contains(focusedWatchable)) {
                                if (passiveWatchables.Contains(focusedWatchable)) {
                                    passiveWatchables.Remove(focusedWatchable);
                                }
                                activeWatchables.Add(focusedWatchable);
                            }
                            break;
                        case EnumStance.Passive:
                            if (!passiveWatchables.Contains(focusedWatchable)) {
                                if (activeWatchables.Contains(focusedWatchable)) {
                                    activeWatchables.Remove(focusedWatchable);
                                }
                                passiveWatchables.Add(focusedWatchable);
                            }
                            break;
                        case EnumStance.Slient:
                            if (passiveWatchables.Contains(focusedWatchable)) {
                                passiveWatchables.Remove(focusedWatchable);
                            }
                            if (activeWatchables.Contains(focusedWatchable)) {
                                activeWatchables.Remove(focusedWatchable);
                            }
                            break;
                    }
                }
                if (needsUpdate) {
                    if (focusedWatchable != null) {
                        frmInstance.BeginInvoke(new UpdateDGVDelegate(UpdateDataGridView), focusedWatchable);
                    }
                }
            }
            
            frmInstance.BeginInvoke(new UpdateLabelDelegate(UpdateLabel), frmInstance.watcherCountlbl, WatchableActiveIndex.ToString());
            frmInstance.BeginInvoke(new UpdateLabelDelegate(UpdateLabel), frmInstance.activeCountlbl, ActiveWatchablesCount.ToString());
            frmInstance.BeginInvoke(new UpdateLabelDelegate(UpdateLabel), frmInstance.passiveCountlbl, PassiveWatchablesCount.ToString());
            needsUpdate = false;
        }

        /// <summary>
        /// Update label method.
        /// </summary>
        /// <param name="parLbl">Label</param>
        /// <param name="parVal">Value</param>
        public void UpdateLabel(Label parLbl, String parVal) {
            parLbl.Text = parVal;
        }

        /// <summary>
        /// Updates the data grid view. 
        /// </summary>
        /// <param name="focusedWatchable">Watchable</param>
        public void UpdateDataGridView(Watchable focusedWatchable) {
            frmInstance.dataGridView1.Rows.Add(focusedWatchable.Name, focusedWatchable.Focus, focusedWatchable.Extent, focusedWatchable.Creation, focusedWatchable.Deletion, focusedWatchable.Rename, focusedWatchable.Change, focusedWatchable.GetStance());
        }

        /// <summary>
        /// Adds a watchable to the watchable array.
        /// </summary>
        /// <param name="parWatchable">Watchable to add</param>
        public void AddWatchable(Watchable parWatchable) {
            watchables[watchableActiveIndex] = parWatchable;
            WatchableActiveIndex++; 
            AFSForm.QueueConsolePost(new ConsolePost("Omnitudal Watcher: ", parWatchable.ToString() + " added to OmnitudalObserver", Color.Magenta, true));
            needsUpdate = true;
        }

        /// <summary>
        /// Removes the watchable at the specified index.
        /// </summary>
        /// <param name="index">Index to remove at</param>
        public void RemoveWatchable(Int32 index) {
            watchables[index] = null;
            AFSForm.QueueConsolePost(new ConsolePost("Omnitudal Observer: ", "Watcher removed from index: " + index, Color.Magenta, true));
        }

        /// <summary>
        /// Gets a watchable specified by its name.
        /// </summary>
        /// <param name="parName">Name of the watchable to retrieve</param>
        /// <returns>Requested watchable, else NULL</returns>
        public Watchable GetWatchable(String parName) {
            Watchable focusable = null;
            Boolean found = false;
            for (Int32 i = 0; i < watchables.Length; i++) {
                if (watchables[i] != null) {
                    if (watchables[i].Name == parName) {
                        focusable = watchables[i];
                        found = true;
                    }
                }
                if (found) break;
            }
            if (!found) {
                AFSForm.QueueConsolePost(new ConsolePost("AFS: ", "Watcher named \"" + parName + "\" does not exist!", Color.OrangeRed, true));
            }
            return focusable;
        }

        /// <summary>
        /// Gets the index of the specified watcher.
        /// </summary>
        /// <param name="parName">Watcher name</param>
        /// <returns>Watchables array index</returns>
        public Int32 GetWatchableIndex(String parName) {
            Int32 returnable = -1;
            Boolean found = false;
            for (Int32 i = 0; i < watchables.Length; i++) {
                if (watchables[i] != null) {
                    if (watchables[i].Name == parName) {
                        returnable = i;
                        found = true;
                    }
                }
                if (found) break;
            }
            if (!found) {
                AFSForm.QueueConsolePost(new ConsolePost("AFS: ", "Watcher named \"" + parName + "\" does not exist!", Color.OrangeRed, true));
            }
            return returnable;
        }

        /// <summary>
        /// Gets the watchable active index.
        /// </summary>
        public Int32 WatchableCount {
            get { return watchableActiveIndex; }
        }

        /// <summary>
        /// Gets and set the watchable active index.
        /// </summary>
        public Int32 WatchableActiveIndex {
            get { return watchableActiveIndex; }
            set { watchableActiveIndex = value; }
        }

        /// <summary>
        /// Gets the active watchable count.
        /// </summary>
        public Int32 ActiveWatchablesCount {
            get { return activeWatchables.Count; }
        }

        /// <summary>
        /// Gets the passive watchable count.
        /// </summary>
        public Int32 PassiveWatchablesCount {
            get { return passiveWatchables.Count; }
        }
    }
}
