﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AllusionFileSystem
{
    public partial class New_Watcher : Form
    {
        private AFSForm instance;

        public New_Watcher(AFSForm instance) {
            InitializeComponent();
            this.instance = instance;
        }

        private void buttonCreate_Click(object sender, EventArgs e) {
            if (Directory.Exists(textBoxDirectory.Text)) {
                AFSForm.QueueCommand("addwatcher " + textBoxName.Text + " " + textBoxDirectory.Text);
                AFSForm.QueueCommand("setstance " + textBoxName.Text + " " + comboBoxStance.SelectedItem.ToString());
                AFSForm.QueueCommand("setextent " + textBoxName.Text + " " + radioButtonTopDown.Checked);
                if (checkBoxCreate.Checked) {
                    AFSForm.QueueCommand("register " + textBoxName.Text + " creation");
                }
                if (checkBoxDelete.Checked) {
                    AFSForm.QueueCommand("register " + textBoxName.Text + " deletion");
                }
                if (checkBoxRename.Checked) {
                    AFSForm.QueueCommand("register " + textBoxName.Text + " rename");
                }
                if (checkBoxChange.Checked) {
                    AFSForm.QueueCommand("register " + textBoxName.Text + " modification");
                }
            }
            this.Close();
        }

        private void buttonSelectDir_Click(object sender, EventArgs e) {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowNewFolderButton = true;
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                this.textBoxDirectory.Text = fbd.SelectedPath;
            }            
            this.DialogResult = System.Windows.Forms.DialogResult.None;
        }

        private void textBoxDirectory_TextChanged(object sender, EventArgs e) {
            if (Directory.Exists(((TextBox)sender).Text)) {
                pictureBoxGoodDir.Image = AllusionFileSystem.Properties.Resources.tick;
                buttonCreate.Enabled = true;
            } else {
                pictureBoxGoodDir.Image = AllusionFileSystem.Properties.Resources.cross;
                buttonCreate.Enabled = false;
            }
        }

        private void New_Watcher_Load(object sender, EventArgs e) {
            comboBoxStance.SelectedIndex = 0;
            buttonCreate.Enabled = false;
        }
    }
}
