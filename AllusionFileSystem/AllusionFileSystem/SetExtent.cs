﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AllusionFileSystem
{
    public partial class SetExtent : Form
    {
        private String watchableName;

        public SetExtent(String watchableName) {
            InitializeComponent();
            this.watchableName = watchableName;
        }

        private void buttonSet_Click(object sender, EventArgs e) {
            AFSForm.QueueCommand("setextent " + watchableName + " " + radioButtonTopDown.Checked.ToString());
            AFSForm.QueueConsolePost(new ConsolePost("setextent " + watchableName + " " + radioButtonTopDown.Checked.ToString(), "", Color.Green, true));
            this.Close();
        }
    }
}
