﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllusionFileSystem
{
    public enum EnumNotificationEventType
    {
        ObjectCreated, ObjectRemoved, ObjectChanged, ObjectRenamed, ApplicationForced, NewWatchableFocus, CRIT_ERROR
    };
}
